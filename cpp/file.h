%{Cpp:LicenseTemplate}\
#pragma once
%{JS: QtSupport.qtIncludes([ ( '%{IncludeQObject}' )          ? 'QtCore/%{IncludeQObject}'                 : '',
                             ( '%{IncludeQWidget}' )          ? 'QtGui/%{IncludeQWidget}'                  : '',
                             ( '%{IncludeQMainWindow}' )      ? 'QtGui/%{IncludeQMainWindow}'              : '',
                             ( '%{IncludeQDeclarativeItem}' ) ? 'QtDeclarative/%{IncludeQDeclarativeItem}' : '',
                             ( '%{IncludeQSharedData}' )      ? 'QtCore/QSharedDataPointer'                : '' ],
                           [ ( '%{IncludeQObject}' )          ? 'QtCore/%{IncludeQObject}'                 : '',
                             ( '%{IncludeQWidget}' )          ? 'QtWidgets/%{IncludeQWidget}'              : '',
                             ( '%{IncludeQMainWindow}' )      ? 'QtWidgets/%{IncludeQMainWindow}'          : '',
                             ( '%{IncludeQDeclarativeItem}' ) ? 'QtQuick1/%{IncludeQDeclarativeItem}'      : '',
                             ( '%{IncludeQQuickItem}' )       ? 'QtDeclarative/%{IncludeQQuickItem}'       : '',
                             ( '%{IncludeQSharedData}' )      ? 'QtCore/QSharedDataPointer'                : '' ])}\
%{JS: Cpp.openNamespaces('%{Class}')}
@if '%{IncludeQSharedData}'
class %{CN}Data;

@endif
@if '%{Base}'
struct %{CN} : public %{Base}{
@else
struct %{CN}{
@endif
@if %{isQObject}
    Q_OBJECT
    public:
@endif
@if '%{Base}' === 'QObject'
%{CN}(QObject *parent) : QObject(parent)%{JS: ('%{SharedDataInit}') ? ', %{SharedDataInit}' : ''}{

}
@elsif '%{Base}' === 'QWidget' || '%{Base}' === 'QMainWindow'
%{CN}(QWidget *parent) : %{Base}(parent)%{JS: ('%{SharedDataInit}') ? ', %{SharedDataInit}' : ''}{

}
@else
%{CN}()%{JS: ('%{SharedDataInit}') ? ' : %{SharedDataInit}' : ''}{

}
@endif
@if '%{IncludeQSharedData}'
    %{CN}(const %{CN} &);
    %{CN} &operator=(const %{CN} &);
    ~%{CN}();
@endif
@if %{isQObject}

signals:

public slots:
@endif
@if '%{IncludeQSharedData}'

private:
    QSharedDataPointer<%{CN}Data> data;
@endif
};

@if '%{IncludeQSharedData}'
class %{CN}Data : public QSharedData{
public:

};

@endif

@if '%{IncludeQSharedData}'

%{CN}::%{CN}(const %{CN} &rhs) : data(rhs.data){

}

%{CN} &%{CN}::operator=(const %{CN} &rhs){
    if (this != &rhs)
        data.operator=(rhs.data);
    return *this;
}

%{CN}::~%{CN}(){

}
@endif
%{JS: Cpp.closeNamespaces('%{Class}')}\
