# list of files involved in creator personalisation for NSA:
# ~/.config/QtProject/qtcreator/snippets/snippets.xml
# ~/.config/QtProject/qtcreator/styles/nsa.xml
# ~/.config/QtProject/qtcreator/templates/wizards/classes/cpp (whole folder)
# ~/.config/QtProject/qtcreator/codestyles (whole folder)

errorStr='usage: qtcreator cfg path ex:"~/.config/QtProject/qtcreator", instruction:[clone/apply]'

if [ $# -lt 2 ] ; then
    echo $errorStr
    exit 0
fi

if [ -d "$1/mimetypes" ] 
then
    echo "qtcreator config path found: \"$1\"" 
else
    echo "qtcreator config path not found, exiting"
    echo $errorStr
    exit 0
fi

if [ "$2" != "clone" ] && [ "$2" != "apply" ]; then
    echo "bad instruction: $2"
    echo $errorStr
    exit 0
fi

if [ "$2" == "clone" ]; then
    echo "$2: /styles"
    cp -rf "$1/styles" `dirname "$0"`

    echo "$2: /codestyles"
    cp -rf "$1/codestyles" `dirname "$0"`

    echo "$2: /snippets"
    cp -rf "$1/snippets" `dirname "$0"`

    echo "$2: /templates/wizards/classes/cpp"
    cp -rf "$1/templates/wizards/classes/cpp" `dirname "$0"`

fi

if [ "$2" == "apply" ]; then
    echo "$2: /styles"
    cp -rf "`dirname "$0"`/styles" "$1"

    echo "$2: /codestyles"
    cp -rf "`dirname "$0"`/codestyles" "$1"

    echo "$2: /snippets"
    cp -rf "`dirname "$0"`/snippets" "$1"

    mkdir "$1/templates"
    mkdir "$1/templates/wizards"
    mkdir "$1/templates/wizards/classes"
    echo "$2: /templates/wizards/classes/cpp"
    cp -rf "`dirname "$0"`/cpp" "$1/templates/wizards/classes"
fi